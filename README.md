# My Neovim Configuration

Welcome to my personal Neovim configuration repository. 
This setup is tailored to my own workflow and preferences. 
It includes a variety of custom key mappings, plugins, and settings that enhance my daily usage of Neovim.

## Disclaimer
This Neovim configuration is provided as-is and is tailored to my personal preferences and workflow. 
It is not guaranteed to fit everyone's needs or work without adjustments in different environments.

Use at your own risk. I am not responsible for any issues, data loss, or damages that may occur from using this configuration. 
Feel free to fork, modify, and adjust the settings to your liking.

## Structure

The configuration is organized into modular Lua files for clarity and ease of maintenance:

- `init.lua`: The entry point that loads all configuration modules.
- `user/options.lua`: Basic Neovim options and settings.
- `user/keymaps.lua`: Custom key mappings for an efficient editing workflow.
- `user/autocommands.lua`: Autocommands for automating tasks based on specific events.
- `user/plugins.lua`: The list of plugins I use.
- `user/cmp.lua`: Configuration for the `nvim-cmp` autocomplete plugin.

## Key Mappings

A brief overview of some key mappings included in this configuration:

- **Window Navigation**: Use Ctrl + Arrow keys to navigate between split windows.
- **Buffer Navigation**: Shift + l/h or Shift + Right/Left Arrow to cycle through open buffers.
- **Resizing Panes**: Alt + Shift + Arrow keys to resize window panes.
- **Quick Exiting**: Ctrl + w to close the current window or buffer.
- **Fast Escape**: In insert mode, press `jk` quickly to return to normal mode.

And many more for efficient navigation, editing, and managing of files and buffers.

## Usage

To use this configuration, clone the repository into your `~/.config/nvim` directory. 
Ensure you have Neovim installed and that it supports Lua configuration (Neovim 0.5 and above).

```sh
git clone https://gitlab.com/mudlej/nvim-config.git ~/.config/nvim

