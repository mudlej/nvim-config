local opts = { noremap = true, silent = true }
local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

--[[ 
- Modes:
  - normal_mode = "n",
  - insert_mode = "i",
  - visual_mode = "v",
  - visual_block_mode = "x",
  - term_mode = "t",
  - command_mode = "c",
--]]

-- Normal --
-- Better window navigation
keymap("n", "<C-Left>", "<C-w>h", opts)
keymap("n", "<C-Down>", "<C-w>j", opts)
keymap("n", "<C-Up>", "<C-w>k", opts)
keymap("n", "<C-Right>", "<C-w>l", opts)

keymap("n", "<A-1>", ":Lex 30<cr>", opts)

-- Resize with arrows
keymap("n", "<A-S-Up>", ":resize +2<CR>", opts)
keymap("n", "<A-S-Down>", ":resize -2<CR>", opts)
keymap("n", "<A-S-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<A-S-Right>", ":vertical resize +2<CR>", opts)

-- Navigate buffers
keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-Right>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)
keymap("n", "<S-Left>", ":bprevious<CR>", opts)

-- Tabs
keymap("n", "<C-t>", ":tabe<CR>", opts)
keymap("n", "<C-w>", ":q<CR>", opts)
--keymap("n", "<C-i>", ":tabnext<CR>", opts)        -- this is will mess with text completion using Tab.
--keymap("n", "<C-S-Tab>", ":tabprevious<CR>", opts)

-- Insert --
-- Press jk fast to enter
keymap("i", "jk", "<ESC>", opts)

-- Visual --
-- Stay in indent mode
keymap("v", "<Tab>", ">gv^", opts)
keymap("v", "<S-Tab>", "<gv^", opts)

keymap("v", ">", ">gv^", opts)
keymap("v", "<", "<gv^", opts)

-- Delete without cutting
keymap('n', '<C-l>', '"_dd', {noremap = true, silent = true})

-- Move text up and down
keymap("v", "<S-A-Down>", ":m '>+1<CR>gv=gv", opts)
keymap("v", "<S-A-Up>", ":m '<-2<CR>gv=gv", opts)
keymap("v", "p", '"_dP', opts)

-- Visual Block --
-- Move text up and down
keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap("x", "K", ":move '<-2<CR>gv-gv", opts)
keymap("x", "Down", ":move '>+1<CR>gv-gv", opts)
keymap("x", "Up", ":move '<-2<CR>gv-gv", opts)
keymap("x", "<S-A-Down>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<S-A-Up>", ":move '<-2<CR>gv-gv", opts)

-- Terminal --
-- Better terminal navigation
keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)

keymap("t", "<C-Left>", "<C-\\><C-N><C-w>h", term_opts)
keymap("t", "<C-Down>", "<C-\\><C-N><C-w>j", term_opts)
keymap("t", "<C-Up>", "<C-\\><C-N><C-w>k", term_opts)
keymap("t", "<C-Right>", "<C-\\><C-N><C-w>l", term_opts)
