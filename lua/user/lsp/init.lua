local status, _ = pcall(require, "lspconfig")
if not status then
    vim.notify("Cannot import 'lspconfig'")
end

require "user.lsp.mason"
require("user.lsp.handlers").setup()
require "user.lsp.none-ls"
