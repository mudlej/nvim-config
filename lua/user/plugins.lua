-- Install Lazy.nvim (github.com/folke/lazy.nvim)
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Use a protected call so we don't error out on first use
local status_ok, lazy = pcall(require, "lazy")
if not status_ok then
  vim.notify("Failed to import Lazy!", vim.log.levels.ERROR)
  return
end

-- configuration for lazy itself.
local lazy_opts = {
  ui = {
    border = "rounded",
    title = "Plugin Manager",
    title_pos = "center",
  },
}

lazy.setup({
  "folke/which-key.nvim",
  { "folke/neoconf.nvim", cmd = "Neoconf" },
  "folke/neodev.nvim",

  -- Colors Schemes
    "lunarvim/colorschemes",
    "folke/tokyonight.nvim",
    "lunarvim/lunar.nvim",
  {
    -- the chosen colorscheme should be available when starting Neovim
    "lunarvim/lunar.nvim",
    lazy = false,           -- make sure we load this during startup if it is your main colorscheme
    priority = 1000,        -- make sure to load this before all the other start plugins
    config = function()
      -- load the colorscheme here
      --vim.cmd([[colorscheme onedarker]])
      vim.cmd([[colorscheme lunar]])
    end,
  },

	-- Completion (cmp) 
    {
      "hrsh7th/nvim-cmp",
      event = { "InsertEnter", "CmdlineEnter" },
      dependencies = {
        "cmp-nvim-lsp",
        "cmp_luasnip",
        "cmp-buffer",
        "cmp-path",
        "cmp-cmdline",
      },
    },
  { "hrsh7th/cmp-nvim-lsp", lazy = true },
  { "saadparwaiz1/cmp_luasnip", lazy = true },
  { "hrsh7th/cmp-buffer", lazy = true },
  { "hrsh7th/cmp-path", lazy = true },
  { "hrsh7th/cmp-cmdline", lazy = true },

  -- Snippets
  {
    "L3MON4D3/LuaSnip",
    event = "InsertEnter",
    dependencies = {
      "friendly-snippets",
    },
  },
  { "rafamadriz/friendly-snippets", lazy = true },

  -- LSP 
   {
    "neovim/nvim-lspconfig",
    lazy = false,
    dependencies = { "mason-lspconfig.nvim", "nlsp-settings.nvim" },
  },
  {
    "williamboman/mason-lspconfig.nvim",
    cmd = { "LspInstall", "LspUninstall" },
    config = function()
      require("mason-lspconfig").setup()
      require("mason-lspconfig").setup {
        ensure_installed = { },
      }
    end,
    lazy = true,
    event = "User FileOpened",
    dependencies = "mason.nvim",
  },
  { "tamago324/nlsp-settings.nvim", cmd = "LspSettings", lazy = true },
  {
    "williamboman/mason.nvim",
    cmd = { "Mason", "MasonInstall", "MasonUninstall", "MasonUninstallAll", "MasonLog" },
    config = function()
        require("mason").setup()
      end,
    build = function()
      pcall(function()
        require("mason-registry").refresh()
      end)
    end,
    event = "User FileOpened",
    lazy = true,
  },
  { "nvimtools/none-ls.nvim", lazy = true },
  { "mfussenegger/nvim-dap", lazy = true },
  { "rcarriga/nvim-dap-ui", lazy = true },
  { "nvim-lua/plenary.nvim", lazy = true },
}, lazy_opts)

